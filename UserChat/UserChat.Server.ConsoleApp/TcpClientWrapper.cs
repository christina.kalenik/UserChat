﻿using System.Net.Sockets;

public class TcpClientWrapper : ITcpClient
{
    private readonly TcpClient _client;

    public TcpClientWrapper(TcpClient client)
    {
        _client = client;
    }

    public Stream GetStream()
    {
        return _client.GetStream();
    }

    public void Close()
    {
        _client.Close();
    }

    public bool Connected => _client.Connected;
}