﻿using System.Net.Sockets;

public class ConnectedClient
{
    public TcpClient Client { get; }
    public string Name { get; }

    public ConnectedClient(TcpClient client, string name)
    {
        Client = client;
        Name = name;
    }
}