﻿namespace UserChat.Server.ConsoleApp.DtoModels
{
    public class ChatUser
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public int ChatId { get; set; }
    }
}
