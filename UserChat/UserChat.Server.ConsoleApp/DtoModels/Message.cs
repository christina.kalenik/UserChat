﻿namespace UserChat.Server.ConsoleApp.DtoModels
{
    public class Message
    {
        public int Id { get; set; }
        public string? Text { get; set; }
        public DateTime CreatedAt { get; set; }
        public string? UserName { get; set; }
    }
}
