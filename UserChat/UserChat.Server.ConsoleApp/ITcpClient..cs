﻿public interface ITcpClient
{
    Stream GetStream();
    void Close();
    bool Connected { get; }
}