﻿using Npgsql;
using System.Net;
using System.Net.Sockets;
using System.Text.Json;
using UserChat.Domain.Enums;
using UserChat.Server.ConsoleApp.DtoModels;

namespace UserChat.Server.ConsoleApp
{
    public class Program
    {
        public static TcpListener listener = new TcpListener(IPAddress.Any, 51686);
        public static List<ConnectedClient> clients = new List<ConnectedClient>();
        public static IDbConnection dbConnection;
        private static void Main(string[] args)
        {
            string connectionString = "Host=localhost:5432;Username=postgres;Password=123456;Database=chat";

            try
            {
                dbConnection = new NpgsqlConnection(connectionString);
                dbConnection.Open();
                Console.WriteLine("Start database.");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Databese error: {ex.Message}");
                return;
            }

            listener.Start();

            try 
            {
                while (true)
                {
                    var client = listener.AcceptTcpClient();

                    Task.Factory.StartNew(() =>
                    {
                        TryRegisterNewClient(client);

                        ReceiveMessagesInLoop(client);
                    });
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                if (listener != null)
                    listener.Stop();
            }
        }

        public static void TryRegisterNewClient(TcpClient client)
        {
            if (client == null)
                return;

            var streamReader = new StreamReader(client.GetStream());
            while (client.Connected)
            {
                var receivedLine = streamReader.ReadLine();

                if (string.IsNullOrEmpty(receivedLine))
                    return;

                var restoredMessage = JsonSerializer.Deserialize<ChatMessageModel>(receivedLine);
                var nickName = restoredMessage?.AuthorsName;

                if (!string.IsNullOrEmpty(nickName))
                {
                    var alreadyExistingClientWithSuchNickName = clients.FirstOrDefault(c => c.Name == nickName);

                    if (alreadyExistingClientWithSuchNickName == null)
                    {
                        if (!UserExistsInDatabase(nickName))
                        {
                            SaveUserToDatabase(nickName);
                        }

                        clients.Add(new ConnectedClient(client, nickName));

                        var serverMessage = new ChatMessageModel()
                        {
                            AuthorsName = "Server",
                            Message = $"New user joined chat: {nickName}",
                            MessageType = MessageType.NewUserConnected
                        };

                        var messageJsonString = JsonSerializer.Serialize(serverMessage, typeof(ChatMessageModel));

                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine($"[{serverMessage?.TimeStamp}] {serverMessage?.Message}");
                        Console.ResetColor();

                        SendMessageToAllClients(messageJsonString);

                        SendChatHistoryToClient(client, 1);
                        break;
                    }
                    else
                    {
                        SendUserAlreadyInChatMessage(client, nickName);
                    }
                }
            }
        }


        public static void ReceiveMessagesInLoop(TcpClient client)
        {
            while (client.Connected)
            {
                try
                {
                    var streamReader = new StreamReader(client.GetStream());
                    var receivedLine = streamReader.ReadLine();

                    if (!string.IsNullOrEmpty(receivedLine))
                    {
                        var restoredMessage = JsonSerializer.Deserialize<ChatMessageModel>(receivedLine);
                        var nickName = restoredMessage?.AuthorsName ?? string.Empty;

                        switch (restoredMessage?.MessageType)
                        {
                            case MessageType.UserDisconnected:
                                HandleUserDisconnect(restoredMessage, nickName);
                                break;
                            case MessageType.DeleteMessage:
                                int messageId = int.Parse(restoredMessage.Message);
                                DeleteMessageFromDatabase(messageId);
                                SendMessageToAllClients($"Message {messageId} has been deleted by {nickName}.");
                                break;
                            default:
                                if (restoredMessage != null)
                                {
                                Console.ForegroundColor = ConsoleColor.Magenta;
                                Console.Write(nickName);
                                Console.ResetColor();
                                    Console.WriteLine($": {restoredMessage.Message} [{restoredMessage.TimeStamp:H:mm:ss}]");

                                    SaveMessageToDatabase(nickName, restoredMessage.Message, 1, out messageId);
                                    restoredMessage.Id = messageId;
                                    var messageJsonString = JsonSerializer.Serialize(restoredMessage, typeof(ChatMessageModel));
                                    SendMessageToAllClients(messageJsonString);
                                }
                                break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Error processing message: {ex.Message}");
                }
            }
        }

        private static void HandleUserDisconnect(ChatMessageModel restoredMessage, string nickName)
        {
            var clientWithSuchNickName = clients.FirstOrDefault(c => c.Name == nickName);
            if (clientWithSuchNickName != null)
            {
                var serverMessage = new ChatMessageModel()
                {
                    AuthorsName = "Server",
                    Message = $"User \"{nickName}\" left chat",
                    MessageType = MessageType.UserDisconnected
                };

                var messageJsonString = JsonSerializer.Serialize(serverMessage, typeof(ChatMessageModel));

                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"[{serverMessage?.TimeStamp:H:mm}] {serverMessage?.Message}");
                Console.ResetColor();

                SendMessageToAllClients(messageJsonString);
                clients.Remove(clientWithSuchNickName);
            }
        }

        public static void SendMessageToAllClients(string message)
        {
            for (int i = 0; i < clients.Count; i++)
            {
                try
                {
                    if (clients[i].Client.Connected)
                    {
                        var streamWriter = new StreamWriter(clients[i].Client.GetStream())
                        {
                            AutoFlush = true
                        };

                        streamWriter.WriteLine(message);
                    }
                }
                catch
                {
                    //ignored
                }
            }
        }

        public static void SaveUserToDatabase(string name)
        {
            try
            {
                string query = "INSERT INTO users (name, chat_id) VALUES (@name, 1)";
                using (var cmd = new NpgsqlCommand(query, (NpgsqlConnection)dbConnection))
                {
                    cmd.Parameters.AddWithValue("name", name);
                    cmd.ExecuteNonQuery();
                }
                Console.WriteLine($"User \"{name}\" saved in database.");
            }
            catch (Npgsql.PostgresException ex) when (ex.SqlState == "23505")
            {
                Console.WriteLine($"Error saving user \"{name}\": The name is already taken.");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error saving user \"{name}\": {ex.Message}");
            }
        }

        public static ChatUser? GetUserByNickname(string name)
        {
            try
            {
                string query = "SELECT id, name, chat_id FROM users WHERE name = @name";
                using (var cmd = new NpgsqlCommand(query, (NpgsqlConnection)dbConnection))
                {
                    cmd.Parameters.AddWithValue("name", name);

                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            var user = new ChatUser
                            {
                                Id = reader.GetInt32(0),
                                Name = reader.GetString(1),
                                ChatId = reader.GetInt32(2)
                            };

                            Console.WriteLine($"User \"{name}\" found in the database.");
                            return user;
                        }
                        else
                        {
                            Console.WriteLine($"User \"{name}\" not found in the database.");
                            return null;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error retrieving user \"{name}\": {ex.Message}");
                return null;
            }
        }

        public static bool UserExistsInDatabase(string name)
        {
            try
            {
                string query = "SELECT COUNT(*) FROM users WHERE name = @name";
                using (var cmd = new NpgsqlCommand(query, (NpgsqlConnection)dbConnection))
                {
                    cmd.Parameters.AddWithValue("name", name);
                    var count = (long)cmd.ExecuteScalar();
                    return count > 0;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error checking user existence \"{name}\": {ex.Message}");
                return false;
            }
        }

        public static void SendUserAlreadyInChatMessage(TcpClient client, string name)
        {
            var streamWriter = new StreamWriter(client.GetStream());
            streamWriter.AutoFlush = true;

            var serverMessage = new ChatMessageModel()
            {
                AuthorsName = "Server",
                Message = $"A user with the name \"{name}\" is already in the chat",
                MessageType = MessageType.UserAlreadyExists
            };

            var messageJsonString = JsonSerializer.Serialize(serverMessage, typeof(ChatMessageModel));

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"[{serverMessage?.TimeStamp:H:mm}] {serverMessage?.Message}");
            Console.ResetColor();

            streamWriter.WriteLine(messageJsonString);
            client.Client.Disconnect(false);
        }

        public static void SaveMessageToDatabase(string name, string messageText, int chatId, out int messageId)
        {
            messageId = 0;

            try
            {
                string getUserQuery = "SELECT id FROM users WHERE name = @name";
                int userId;
                using (var getUserCmd = new NpgsqlCommand(getUserQuery, (NpgsqlConnection)dbConnection))
                {
                    getUserCmd.Parameters.AddWithValue("name", name);
                    userId = (int)getUserCmd.ExecuteScalar();
                }

                string insertMessageQuery = "INSERT INTO messages (user_id, text, chat_id) VALUES (@user_id, @text, @chat_id) RETURNING id";
                using (var insertCmd = new NpgsqlCommand(insertMessageQuery, (NpgsqlConnection)dbConnection))
                {
                    insertCmd.Parameters.AddWithValue("user_id", userId);
                    insertCmd.Parameters.AddWithValue("text", messageText);
                    insertCmd.Parameters.AddWithValue("chat_id", chatId);
                    var id = insertCmd.ExecuteScalar();

                    int.TryParse(id?.ToString(), out messageId);
                }
                Console.WriteLine($"Message from user \"{name}\" saved in database.");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error saving message from user \"{name}\": {ex.Message}");
            }
        }

        public static void SendChatHistoryToClient(TcpClient client, int chatId)
        {
            var messages = GetMessagesFromDatabase(chatId);
            var streamWriter = new StreamWriter(client.GetStream());
            streamWriter.AutoFlush = true;

            foreach (var message in messages)
            {
                var chatMessage = new ChatMessageModel(
                    id: message.Id,
                    authorsName: message.UserName ?? string.Empty,
                    message: message.Text ?? string.Empty,
                    timeStamp: message.CreatedAt,
                    messageType: MessageType.ChatMessage
                );

                var messageJsonString = JsonSerializer.Serialize(chatMessage, typeof(ChatMessageModel));
                streamWriter.WriteLine(messageJsonString);
            }
        }

        public static List<Message> GetMessagesFromDatabase(int chatId)
        {
            var messages = new List<Message>();

            try
            {
                string query = "SELECT m.id, m.text, m.created_at, u.name FROM messages m JOIN users u ON m.user_id = u.id WHERE m.chat_id = @chatId ORDER BY m.created_at";
                using (var cmd = dbConnection.CreateCommand())
                {
                    cmd.CommandText = query;
                    var chatIdParam = cmd.CreateParameter();
                    chatIdParam.ParameterName = "chatId";
                    chatIdParam.Value = chatId;
                    cmd.Parameters.Add(chatIdParam);

                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var message = new Message
                            {
                                Id = reader.GetInt32(0),
                                Text = reader.GetString(1),
                                CreatedAt = reader.GetDateTime(2),
                                UserName = reader.GetString(3)
                            };
                            messages.Add(message);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error retrieving messages: {ex.Message}");
            }

            return messages;
        }

        public static void UpdateMessageInDatabase(int messageId, string newText)
        {
            try
            {
                string query = "UPDATE messages SET text = @text, updated_at = NOW() WHERE id = @messageId";
                using (var cmd = dbConnection.CreateCommand())
                {
                    cmd.CommandText = query;
                    var textParam = cmd.CreateParameter();
                    textParam.ParameterName = "text";
                    textParam.Value = newText;
                    cmd.Parameters.Add(textParam);

                    var messageIdParam = cmd.CreateParameter();
                    messageIdParam.ParameterName = "messageId";
                    messageIdParam.Value = messageId;
                    cmd.Parameters.Add(messageIdParam);

                    cmd.ExecuteNonQuery();
                }
                Console.WriteLine($"Message {messageId} updated in database.");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error updating message: {ex.Message}");
            }
        }

        public static void DeleteMessageFromDatabase(int messageId)
        {
            try
            {
                string query = "DELETE FROM messages WHERE id = @messageId";
                using (var cmd = dbConnection.CreateCommand())
                {
                    cmd.CommandText = query;
                    var messageIdParam = cmd.CreateParameter();
                    messageIdParam.ParameterName = "messageId";
                    messageIdParam.Value = messageId;
                    cmd.Parameters.Add(messageIdParam);

                    cmd.ExecuteNonQuery();
                }
                Console.WriteLine($"Message {messageId} deleted from database.");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error deleting message: {ex.Message}");
            }
        }
        /*  public static void UpdateMessageInDatabase(int messageId, string newText)
          {
              try
              {
                  string query = "UPDATE messages SET text = @text, updated_at = NOW() WHERE id = @messageId";
                  using (var cmd = new NpgsqlCommand(query, (NpgsqlConnection)dbConnection))
                  {
                      cmd.Parameters.AddWithValue("text", newText);
                      cmd.Parameters.AddWithValue("messageId", messageId);
                      cmd.ExecuteNonQuery();
                  }
                  Console.WriteLine($"Message {messageId} updated in database.");
              }
              catch (Exception ex)
              {
                  Console.WriteLine($"Error updating message: {ex.Message}");
              }
          }

          public static void DeleteMessageFromDatabase(int messageId)
          {
              try
              {
                  string query = "DELETE FROM messages WHERE id = @messageId";
                  using (var cmd = new NpgsqlCommand(query, (NpgsqlConnection)dbConnection))
                  {
                      cmd.Parameters.AddWithValue("messageId", messageId);
                      cmd.ExecuteNonQuery();
                  }
                  Console.WriteLine($"Message {messageId} deleted from database.");
              }
              catch (Exception ex)
              {
                  Console.WriteLine($"Error deleting message: {ex.Message}");
              }
          }*/

    }
}




