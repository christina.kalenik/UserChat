﻿using Moq;
using System.Data;
using UserChat.Server.ConsoleApp;
using Xunit;

namespace UserChat.Server.Tests
{
    public class ProgramTests
    {
        [Fact]
        public void TestDatabaseConnectionFailure()
        {
            var connectionString = "Host=localhost:5432;Username=postgres;Password=wrongpassword;Database=chat";
            var mockConnection = new Mock<IDbConnection>();
            mockConnection.Setup(conn => conn.Open()).Throws(new Exception("Database error"));

            var ex = Assert.Throws<Exception>(() =>
            {
                Program.dbConnection = mockConnection.Object;
                Program.dbConnection.Open();
            });

            Assert.Equal("Database error", ex.Message);
        }
        [Fact]
        public void TestDatabaseConnectionSuccess()
        {
            var connectionString = "Host=localhost:5432;Username=postgres;Password=123456;Database=chat";
            var mockConnection = new Mock<IDbConnection>();
            mockConnection.Setup(conn => conn.Open());

            Program.dbConnection = mockConnection.Object;
            Program.dbConnection.Open();

            mockConnection.Verify(conn => conn.Open(), Times.Once());
        }

        [Fact]
        public void DeleteMessageFromDatabase_Success()
        {
            var messageId = 1;
            var mockCommand = new Mock<IDbCommand>();
            var mockConnection = new Mock<IDbConnection>();
            var mockParameter = new Mock<IDbDataParameter>();

            mockConnection.Setup(conn => conn.CreateCommand()).Returns(mockCommand.Object);
            mockCommand.Setup(cmd => cmd.CreateParameter()).Returns(mockParameter.Object);
            mockCommand.Setup(cmd => cmd.ExecuteNonQuery()).Returns(1);

            mockParameter.SetupAllProperties();

            mockCommand.SetupProperty(cmd => cmd.CommandText);
            mockCommand.Object.CommandText = "DELETE FROM messages WHERE id = @messageId";

            mockCommand.Setup(cmd => cmd.Parameters.Add(mockParameter.Object));

            Program.dbConnection = mockConnection.Object;

            Program.DeleteMessageFromDatabase(messageId);

            mockCommand.Verify(cmd => cmd.ExecuteNonQuery(), Times.Once());
            mockCommand.Verify(cmd => cmd.Parameters.Add(It.IsAny<IDbDataParameter>()), Times.Once());
            Assert.Equal("DELETE FROM messages WHERE id = @messageId", mockCommand.Object.CommandText);
            mockParameter.VerifySet(p => p.ParameterName = "messageId", Times.Once());
            mockParameter.VerifySet(p => p.Value = messageId, Times.Once());
            Console.WriteLine("Message deleted successfully.");
        }

        [Fact]
        public void UpdateMessageInDatabase_Success()
        {
            // Arrange
            var messageId = 1;
            var newText = "New message text";
            var mockCommand = new Mock<IDbCommand>();
            var mockConnection = new Mock<IDbConnection>();
            var textParam = new Mock<IDbDataParameter>();
            var messageIdParam = new Mock<IDbDataParameter>();
            var parameters = new Mock<IDataParameterCollection>();

            mockConnection.Setup(conn => conn.CreateCommand()).Returns(mockCommand.Object);

            mockCommand.SetupSequence(cmd => cmd.CreateParameter())
                       .Returns(textParam.Object)
                       .Returns(messageIdParam.Object);

            mockCommand.Setup(cmd => cmd.ExecuteNonQuery()).Returns(1); // Assume the update affects one row

            textParam.SetupAllProperties();
            messageIdParam.SetupAllProperties();

            textParam.Object.ParameterName = "text";
            textParam.Object.Value = newText;
            messageIdParam.Object.ParameterName = "messageId";
            messageIdParam.Object.Value = messageId;

            mockCommand.SetupProperty(cmd => cmd.CommandText);
            mockCommand.Setup(cmd => cmd.Parameters).Returns(parameters.Object);
            parameters.Setup(p => p.Add(It.IsAny<IDbDataParameter>())).Returns(1); // Simulate adding parameters

            Program.dbConnection = mockConnection.Object;

            // Act
            Program.UpdateMessageInDatabase(messageId, newText);

            // Assert
            mockCommand.Verify(cmd => cmd.ExecuteNonQuery(), Times.Once());
            Assert.Equal("UPDATE messages SET text = @text, updated_at = NOW() WHERE id = @messageId", mockCommand.Object.CommandText);
            parameters.Verify(p => p.Add(It.IsAny<IDbDataParameter>()), Times.Exactly(2));
            Console.WriteLine("Message updated successfully in database.");
        }

        //[Fact]
        //public void SaveMessageToDatabase_Success()
        //{
        //    // Arrange
        //    var name = "John Doe";
        //    var messageText = "Hello, this is a test message";
        //    var chatId = 1;
        //    var userId = 10; // Simulated user ID

        //    var mockCommand = new Mock<IDbCommand>();
        //    var mockConnection = new Mock<IDbConnection>();
        //    var mockParameterName = new Mock<IDbDataParameter>();
        //    var mockParameterUserId = new Mock<IDbDataParameter>();
        //    var mockParameterText = new Mock<IDbDataParameter>();
        //    var mockParameterChatId = new Mock<IDbDataParameter>();

        //    mockConnection.Setup(conn => conn.CreateCommand()).Returns(mockCommand.Object);

        //    // Simulate the sequence of creating parameters
        //    mockCommand.SetupSequence(cmd => cmd.CreateParameter())
        //               .Returns(mockParameterName.Object)
        //               .Returns(mockParameterUserId.Object)
        //               .Returns(mockParameterText.Object)
        //               .Returns(mockParameterChatId.Object);

        //    // Setup parameters
        //    mockParameterName.Setup(p => p.ParameterName).Returns("name");
        //    mockParameterName.Setup(p => p.Value).Returns(name);
        //    mockParameterUserId.Setup(p => p.ParameterName).Returns("userId");
        //    mockParameterUserId.Setup(p => p.Value).Returns(userId);
        //    mockParameterText.Setup(p => p.ParameterName).Returns("text");
        //    mockParameterText.Setup(p => p.Value).Returns(messageText);
        //    mockParameterChatId.Setup(p => p.ParameterName).Returns("chatId");
        //    mockParameterChatId.Setup(p => p.Value).Returns(chatId);

        //    // Setting command behavior
        //    mockCommand.Setup(cmd => cmd.ExecuteScalar()).Returns(userId);
        //    mockCommand.Setup(cmd => cmd.ExecuteScalar()).Returns(1);

        //    Program.dbConnection = mockConnection.Object;

        //    // Act
        //    Program.SaveMessageToDatabase(name, messageText, chatId, out var messageId);

        //    // Assert
        //    mockCommand.Verify(cmd => cmd.ExecuteScalar(), Times.Exactly(2), "ExecuteScalar was not called.");
        //    //mockCommand.Verify(cmd => cmd.ExecuteNonQuery(), Times.Once(), "ExecuteNonQuery was not called.");

        //    Console.WriteLine("Message saved successfully in database.");
        //}

    }
}
