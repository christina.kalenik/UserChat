﻿using System.Linq;

namespace UserChat.WpfClient.Helpers
{
    internal static class InputValidationHelper
    {
        public static bool TryValidateIpv4Address(string value)
        {
            if (string.IsNullOrEmpty(value))
                return false;

            // divide input value into 4 sub values
            string[] splitValues = value.Split('.');

            // there should be 4 values and if it is not so, than return false
            if (splitValues.Length != 4)
                return false;

            byte tempForParsing;

            // check if all values can be parsed to byte (0..255)
            return splitValues.All(r => byte.TryParse(r, out tempForParsing));
        }

        public static bool TryValidatePort(string value)
        {
            if (string.IsNullOrEmpty(value))
                return false;

            return ushort.TryParse(value, out _);
        }
    }
}
