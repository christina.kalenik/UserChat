﻿using System.Windows;
using UserChat.WpfClient.ViewModels;
using UserChat.WpfClient.Views;

namespace UserChat.WpfClient
{
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            var mainWindow = new MainWindow()
            {
                DataContext = new MainViewModel("127.0.0.1", 51686, "KristoFirrobin")
            };
            mainWindow.Show();
        }
    }
}
