﻿using CommunityToolkit.Mvvm.Input;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows;
using UserChat.Domain.Enums;
using UserChat.WpfClient.Helpers;
using UserChat.WpfClient.Mvvm;

namespace UserChat.WpfClient.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        private const int MessagePollingIntervalMilliSeconds = 50;

        private TcpClient? _client;
        private StreamReader? _streamReader;
        private StreamWriter? _streamWriter;

        private string _ip = string.Empty;
        private bool _ipValidationSuccess;

        private string _port = string.Empty;
        private bool _portValidationSuccess;

        private string _nick = string.Empty;
        private ObservableCollection<ChatMessageModel> _messages = new();
        private string _message = string.Empty;

        public string IP
        {
            get => _ip;
            set
            {
                if (SetProperty(ref _ip, value))
                    IpValidationSuccess = InputValidationHelper.TryValidateIpv4Address(value);
            }
        }

        public bool IpValidationSuccess
        {
            get => _ipValidationSuccess;
            set
            {
                if (SetProperty(ref _ipValidationSuccess, value))
                    ConnectAsyncCommand?.NotifyCanExecuteChanged();
            }
        }

        public string Port
        {
            get => _port;
            set
            {
                if (SetProperty(ref _port, value))
                    PortValidationSuccess = InputValidationHelper.TryValidatePort(value);
            }
        }

        public bool PortValidationSuccess
        {
            get => _portValidationSuccess;
            set
            {
                if (SetProperty(ref _portValidationSuccess, value))
                    ConnectAsyncCommand?.NotifyCanExecuteChanged();
            }
        }
        public string Nick
        {
            get => _nick;
            set => SetProperty(ref _nick, value);
        }
        public string Message
        {
            get => _message;
            set
            {
                SetProperty(ref _message, value);
                SendAsyncCommand?.NotifyCanExecuteChanged();
            }
        }
        public ObservableCollection<ChatMessageModel> Messages
        {
            get => _messages;
            set => SetProperty(ref _messages, value);
        }

        public AsyncRelayCommand ConnectAsyncCommand { get; }
        public AsyncRelayCommand DisconnectAsyncCommand { get; }
        public AsyncRelayCommand SendAsyncCommand { get; }
        public AsyncRelayCommand<object> DeleteAsyncCommand { get; }

        public RelayCommand MinimizeWindowCommand { get; }
        public RelayCommand ChangeWindowStateCommand { get; }
        public AsyncRelayCommand CloseApplicationAsyncCommand { get; }

        public AsyncRelayCommand<int> DeleteMessageAsyncCommand { get; }



        public MainViewModel(string ip, int port, string nickName)
        {
            IP = ip;
            Port = port.ToString();
            Nick = nickName;

            Messages = new ObservableCollection<ChatMessageModel>();

            ConnectAsyncCommand = new AsyncRelayCommand(ConnectAsync, CanConnect);
            DisconnectAsyncCommand = new AsyncRelayCommand(DisconnectAsync, CanDisconnect);
            SendAsyncCommand = new AsyncRelayCommand(SendAsync, CanSend);
            DeleteAsyncCommand = new AsyncRelayCommand<object>(DeleteAsync);

            MinimizeWindowCommand = new RelayCommand(Minimize);
            ChangeWindowStateCommand = new RelayCommand(ChangeWindowState);
            CloseApplicationAsyncCommand = new AsyncRelayCommand(CloseApplicationAsync);

            StartListeningForMessages();
        }

        private void StartListeningForMessages()
        {
            Task.Factory.StartNew(async () =>
            {
                while (true)
                {
                    try
                    {
                        if (_client?.Connected == true)
                        {
                            var message = _streamReader?.ReadLine();

                            var restoredMessage = JsonSerializer.Deserialize<ChatMessageModel>(message ?? string.Empty);

                            if (restoredMessage != null)
                            {
                                Application.Current.Dispatcher.Invoke(() =>
                                {
                                    Messages.Add(restoredMessage);
                                });
                            }

                            if (restoredMessage?.MessageType == MessageType.UserAlreadyExists)
                            {
                                Application.Current.Dispatcher.Invoke(() =>
                                {
                                    DisconnectAsyncCommand?.Execute(null);
                                });
                            }
                        }

                        await Task.Delay(MessagePollingIntervalMilliSeconds);
                    }
                    catch (Exception)
                    {
                        //ignored
                    }
                }
            });
        }

        private Task ConnectAsync()
        {
            var chatMessageModel = new ChatMessageModel()
            {
                AuthorsName = Nick,
                MessageType = MessageType.NewUserConnected,
            };

            string chatMessageModelJson = JsonSerializer.Serialize(chatMessageModel, typeof(ChatMessageModel));

            return Task.Factory.StartNew(() =>
            {
                try
                {
                    _client = new TcpClient();
                    _client.Connect(IP, ushort.Parse(Port));
                    _streamReader = new StreamReader(_client.GetStream());
                    _streamWriter = new StreamWriter(_client.GetStream());
                    _streamWriter.AutoFlush = true;

                    _streamWriter.WriteLine(chatMessageModelJson);

                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        SendAsyncCommand?.NotifyCanExecuteChanged();
                    });
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                    _client = null;
                }
                finally
                {
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        ConnectAsyncCommand?.NotifyCanExecuteChanged();
                        DisconnectAsyncCommand?.NotifyCanExecuteChanged();
                    });
                }
            });
        }

        private async Task DisconnectAsync()
        {
            var chatMessageModel = new ChatMessageModel()
            {
                AuthorsName = Nick,
                MessageType = MessageType.UserDisconnected
            };

            string chatMessageModelJson = JsonSerializer.Serialize(chatMessageModel, typeof(ChatMessageModel));
            _streamWriter?.WriteLine(chatMessageModelJson);

            // Crutch delay to wait for "disconnect" message received, and only then disconnect
            await Task.Delay(200);

            _client?.Close();
            _client?.Dispose();
            _streamReader?.Dispose();
            _streamWriter?.Dispose();

            Application.Current.Dispatcher.Invoke(() =>
            {
                ConnectAsyncCommand?.NotifyCanExecuteChanged();
                DisconnectAsyncCommand?.NotifyCanExecuteChanged();
            });
        }

        private Task SendAsync()
        {
            var chatMessageModel = new ChatMessageModel()
            {
                AuthorsName = Nick,
                Message = Message,
                MessageType = MessageType.ChatMessage
            };

            string chatMessageModelJson = JsonSerializer.Serialize(chatMessageModel, typeof(ChatMessageModel));

            return Task.Factory.StartNew(() =>
            {
                try
                {
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        _streamWriter?.WriteLine(chatMessageModelJson);
                        Message = string.Empty;
                    });
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            });
        }

        private async Task DeleteAsync(object? messageId)
        {
            if (!int.TryParse(messageId?.ToString(), out int intId))
                return;

            var id = messageId?.ToString();

            if (string.IsNullOrEmpty(id))
                return;

            var result = await TryDeleteMessageAsync(id);

            if (result)
            {
                var message = Messages
                    .FirstOrDefault(message => message.Id == intId);

                if(message != null)
                    Messages.Remove(message);
            }
        }

        private bool CanConnect()
        {
            return _client == null || _client?.Connected == false;
        }

        private bool CanDisconnect()
        {
            return _client != null && _client?.Connected == true;
        }

        private bool CanSend()
        {
            return _client?.Connected == true && !string.IsNullOrWhiteSpace(Message);
        }

        private void Minimize()
        {
            Application.Current.MainWindow.WindowState = WindowState.Minimized;
        }

        private void ChangeWindowState()
        {
            if (Application.Current.MainWindow.WindowState != WindowState.Maximized)
                Application.Current.MainWindow.WindowState = WindowState.Maximized;
            else
                Application.Current.MainWindow.WindowState = WindowState.Normal;
        }

        private async Task CloseApplicationAsync()
        {
            await DisconnectAsync();

            Application.Current?.MainWindow?.Close();
        }

        private async Task<bool> TryDeleteMessageAsync(string messageId)
        {
            var deleteMessageModel = new ChatMessageModel()
            {
                AuthorsName = Nick,
                Message = messageId,
                MessageType = MessageType.DeleteMessage
            };

            string deleteMessageModelJson = JsonSerializer.Serialize(deleteMessageModel, typeof(ChatMessageModel));

            try
            {
                await Task.Run(() =>
                {
                    _streamWriter?.WriteLine(deleteMessageModelJson);
                });

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
    }
}
