﻿using UserChat.Domain.Enums;

public class ChatMessageModel
{
    public int Id { get; set; }
    public string AuthorsName { get; set; } = string.Empty;
    public string Message { get; set; } = string.Empty;
    public DateTime TimeStamp { get; set; } = DateTime.UtcNow;
    public MessageType MessageType { get; set; }

    public ChatMessageModel() { }

    public ChatMessageModel(int id, string authorsName, string message, DateTime timeStamp, MessageType messageType)
    {
        Id = id;
        AuthorsName = authorsName ?? throw new ArgumentNullException(nameof(authorsName));
        Message = message ?? throw new ArgumentNullException(nameof(message));
        TimeStamp = timeStamp;
        MessageType = messageType;
    }
}