﻿

namespace UserChat.Domain.Enums
{
    public enum MessageType
    {
        NewUserConnected,
        UserDisconnected,
        UserAlreadyExists,
        UserAlreadyInChat,
        ChatMessage,
        UpdateMessage,
        DeleteMessage
    }
}